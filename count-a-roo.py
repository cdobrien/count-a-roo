#!/usr/bin/python2

import json
import re
import sys
import time

import requests
import requests.auth

AuthInfoPath = "authinfo"
UserAgent = "count-a-roo 0.1 by cdo"

def readAuthInfo():
    with open(AuthInfoPath, "r") as f:
        lines = f.readlines()
    id = str.strip(lines[0])
    secret = str.strip(lines[1])
    username = str.strip(lines[2])
    password = str.strip(lines[3])
    authInfo = {
        "id": id,
        "secret": secret,
        "username": username,
        "password": password
    }
    return authInfo

def normaliseUri(uri):
    uri = re.sub(r"/?([\?;]context=.*)?$", "", uri)
    return re.sub(r"https?://(.*\.)?reddit", "https://oauth.reddit", uri, 1)

def authenticate(id, secret, username, password):
    AuthEndpoint = "https://www.reddit.com/api/v1/access_token"
    auth = requests.auth.HTTPBasicAuth(id, secret)
    data = {
        "grant_type": "password",
        "username": username,
        "password": password
    }
    header = {
        "User-Agent": UserAgent
    }
    response = requests.post(AuthEndpoint, auth=auth, data=data, headers=header)
    return response.json()["access_token"]

def getComment(token, uri):
    try:
        authorization = "bearer %s" % token
        header = {
            "Authorization": authorization,
            "User-Agent": UserAgent
        }
        response = requests.get(uri, headers=header)
        return response.json()[1]["data"]["children"][0]["data"] # gross
    except:
        print "! failed to get URI \"%s\"" % uri
        return None

def getText(comment):
    match = re.match(r"(.*)\[(.*)\]\s*\(.*\).*", comment["body"])
    return match.group(1) + match.group(2)

def getUri(comment):
    try:
        uri = re.match(r".*\[.*\]\s*\((.*)\).*", comment["body"]).group(1)
        return normaliseUri(uri)
    except:
        print "! no link in comment \"%s\"" % comment["body"]
        return None

def getReply(comment):
    return comment["replies"]["data"]["children"][0]["data"]

def nextARoo(token, uri):
    comment = getComment(token, uri)
    if comment is None:
        return None
    nextUri = getUri(comment)
    depth = 0
    while nextUri is None and depth < 5:
        comment = getReply(comment)
        nextUri = getUri(comment)
        depth += 1
    print comment["body"]
    return nextUri

if __name__ == "__main__":
    authInfo = readAuthInfo()
    token = authenticate(**authInfo)
    uri = normaliseUri(sys.argv[1])
    visited = []
    count = 0
    while True:
        uri = nextARoo(token, uri)
        if uri == None or uri in visited:
            break
        count = count + 1
        time.sleep(1) # docs say no more than 60 requests / min
    print "count: %d" % count
